# frozen_string_literal: true

require_relative 'cinc/version'
require_relative 'cinc/dcs/model/group'
require_relative 'cinc/dcs/model/unit'
require_relative 'cinc/dcs/model/position'
require_relative 'cinc/dcs/api/add_group'

# This is the top-level module for the Command In Chief RPC ruby client.
# It serves as a namespace for all other modules and classes.
module Cinc
  # Your code goes here...
end
