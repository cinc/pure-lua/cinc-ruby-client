# frozen_string_literal: true

require 'json'
require 'socket'

module Cinc
  module DCS
    module API
      # Calls the DCS AddGroup API which creates a new group of units
      #
      # See https://wiki.hoggitworld.com/view/DCS_func_addGroup for more
      # information
      module AddGroup
        class << self
          def call(criteria)
            s = TCPSocket.open('192.168.1.25', 9001)
            s.puts <<EOC
          {"name":"add_group","arguments":{"country_id":#{criteria.country_id}, "group_category":#{criteria.group_category},"group_data":#{criteria.group.to_json} }}
EOC
            result = s.gets
            s.close
            result
          end
        end

        class Criteria
          # country_id Numeric Country ID - see https://wiki.hoggitworld.com/view/DCS_enum_country
          # group_category Numeric group_category, see https://wiki.hoggitworld.com/view/DCS_Class_Group
          # group: Model::Group instance

          attr_accessor :country_id, :group_category, :group

          def initialize(country_id:, group_category:, group:)
            self.country_id = country_id
            self.group_category = group_category
            self.group = group
          end
        end
      end
    end
  end
end
