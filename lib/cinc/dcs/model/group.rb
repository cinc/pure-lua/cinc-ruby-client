# frozen_string_literal: true

require 'json'

module Cinc
  module DCS
    module Model
      class Group
        attr_accessor :country_id, # Numeric Country ID - see https://wiki.hoggitworld.com/view/DCS_enum_country
                      :group_category, # Numeric group_category, see https://wiki.hoggitworld.com/view/DCS_Class_Group
                      :visible, # Is the group visible before the start
                      :late_activation, # Boolean: Is the group late activation
                      :tasks, # An array of tasks
                      :task, # ???
                      :task_selected, # Boolean: Is the task delected
                      :route, # An array of points and spans
                      :group_id, # ID of the group
                      :hidden, # Boolean: Is the group visible on the F10 map
                      :units, # Array of units
                      :position, # coordinates of the group
                      :name, # Name of the group
                      :start_time, # Time in seconds from command to group being created
                      :uncontrollable, # Boolean:  Does the group respond to orders via F10
                      :manual_heading, # Boolean: ???
                      :id # ID of the group


        attr_reader :position # starting coordinates of the group

        def initialize(position)
          self.position = position
	  self.route = {
            spans: [],
            points: [
            # The Initial Point which is the same as the starting location
              {
                name: "Start",
                type: "Turning Point",
                x: position.lat,
                y: position.lon
              }
            ]
          }
	  self.units = []
        end

        def to_json
          {
            visible: visible,
            tasks: tasks,
            uncontrollable: uncontrollable,
            task: task,
            manualHeading: manual_heading,
            taskSelected: task_selected,
            route: route,
            groupId: id,
            hidden: hidden,
            units: units,
            x: position.lat,
            y: position.lon,
            name: name,
            start_time: start_time
          }.to_json
        end
      end
    end
  end
end
